/*
 * Task_Functions.h
 *
 *  Created on: 12/04/2017
 *      Author: Salvador
 */

#ifndef SOURCE_TASK_FUNCTIONS_H_
#define SOURCE_TASK_FUNCTIONS_H_

#include <string.h>
#include "FreeRTOS.h"
#include "queue.h"
#include "RTC_and_EEPROM_Functions.h"
#include "SPI_functions.h"
#include "General_functions.h"
#include "fsl_uart_freertos.h"
#include "fsl_uart.h"

#define ENTER 0xD
#define FORMATBIT1 0x1

const char *menu_readMem = "\033[2J\033[1;1HDireccion de lectura:  \r\nLongitud en bytes: \r\nContenido: ";
const char *firstpoint_readMem = "\033[1;22H ";
const char *secondpoint_readMem = "\033[2;20H ";
const char *thirdpoint_readMem = "\033[4;1H  ";
const char *completed_readMem = "\033[7;15HPresiona la tecla ESC para continuar...";

const char *menu_writeMem = "\033[2J\033[1;1HDireccion de escritura:  \r\nTexto a guardar:  ";
const char *firstpoint_writeMem = "\033[1;25H ";
const char *secondpoint_writeMem = "\033[2;19H ";
const char *completed_writeMem = "\033[7;15HSu texto ha sido guardado...";

const char *menu_setHour = "\033[2J\033[1;1HEscribir hora en hh:mm:ss: ";
const char *firstpoint_setHour = "\033[1;27H ";
const char *completed_setHour = "\033[2;18HLa hora ha sido cambiada... ";

const char *menu_setDate = "\033[2J\033[1;1HEscribir fecha en dd/mm/aa: ";
const char *firstpoint_setDate = "\033[1;27H ";
const char *completed_setDate = "\033[2;18HLa fecha ha sido cambiada... ";

const char *menu_getHour = "\033[2J\033[1;1HLa hora actual es:";
const char *firstpoint_getHour = "\033[1;20H ";

const char *menu_getDate = "\033[2J\033[1;1HLa fecha actual es:";
const char *firstpoint_getDate = "\033[1;20H ";


const char *menu_echoLCD = "\033[2J\033[1;18H Echo LCD ";
const char *pointer_echoLCD = "\033[2;1H  ";

const char* menu_formatoHora = "\033[2J\033[1;1HEl formato actual es:\r\nDesea cambiar el formato a 24h"
		" si/no? ";
const char* completed_formatoHora = "\033[2;18HEl formato ha sido cambiado";

static uint8_t formatHour;

void readMemory();
void writeMemory();
void setUpHour();
void setUpDate();
void readHour();
void readDate();
void echoLCD();
void formatHours();



#endif /* SOURCE_TASK_FUNCTIONS_H_ */
