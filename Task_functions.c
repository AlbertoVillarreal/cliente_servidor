/*
 * Task_functions.c
 *
 *  Created on: 01/04/2017
 *      Author: Salvador
 */

#include "Task_Functions.h"

uart_rtos_handle_t handle;

void readMemory()
{
	uint8_t buffer_received[6];
	uint8_t buffer_received_bytes[2];
	size_t var;
	uint16_t address = 0;
	uint8_t bytes_number = 0;

	UART_RTOS_Send(&handle, (uint8_t *)menu_readMem, strlen(menu_readMem));

	UART_RTOS_Send(&handle, (uint8_t *)firstpoint_readMem, strlen(firstpoint_readMem));
	UART_RTOS_Receive(&handle,buffer_received,sizeof(buffer_received),&var);

	for(uint8_t i = 2; i<6; i++)
	{
		if((47<buffer_received[i]) && (58>buffer_received[i]))
			address |= (buffer_received[i] - 48) << (4*(6-(i+1)));
		else
			address |= (buffer_received[i] - 55) << (4*(6-(i+1)));
	}

	UART_RTOS_Send(&handle, (uint8_t *)buffer_received, var);

	UART_RTOS_Send(&handle, (uint8_t *)secondpoint_readMem, strlen(secondpoint_readMem));
	UART_RTOS_Receive(&handle,buffer_received_bytes,sizeof(buffer_received_bytes),&var);

	for(uint8_t i = 0; i<2; i++)
	{
		if((47<buffer_received_bytes[i]) && (58>buffer_received_bytes[i]))
			bytes_number |= (buffer_received_bytes[i] - 48) << (4*(2-(i+1)));
		else
			bytes_number |= (buffer_received_bytes[i] - 55) << (4*(2-(i+1)));
	}

	UART_RTOS_Send(&handle, (uint8_t *)buffer_received_bytes, var);

	UART_RTOS_Send(&handle, (uint8_t *)thirdpoint_readMem, strlen(thirdpoint_readMem));
	uint8_t text[bytes_number+1];
	for(uint8_t counter = 0; counter<bytes_number; counter++)
	{
		text[counter] = EEPROM_getData(address);
		address += 0x08;
	}
	text[bytes_number] = ' ';
	text[bytes_number+1] = '\0';

	uint32_t array_length = sizeof(text)/sizeof(text[0]);

	UART_RTOS_Send(&handle, (uint8_t *)text, array_length);

	UART_RTOS_Send(&handle, (uint8_t *)completed_readMem, strlen(completed_readMem));
	while(0x1B != buffer_received_bytes[0])
	{
		UART_RTOS_Receive(&handle,buffer_received_bytes,sizeof(buffer_received_bytes) - 1,&var);
	}
}

void writeMemory()
{
	uint8_t buffer_received[6];
	uint8_t buffer_received_bytes[1];
	size_t var;
	uint16_t address = 0;
	uint8_t cont = 0;
	uint8_t bytes_number = 0;

	UART_RTOS_Send(&handle, (uint8_t *)menu_writeMem, strlen(menu_writeMem));

	UART_RTOS_Send(&handle, (uint8_t *)firstpoint_writeMem, strlen(firstpoint_writeMem));
	UART_RTOS_Receive(&handle,buffer_received,sizeof(buffer_received),&var);

	for(uint8_t i = 2; i<6; i++)
	{
		if((47<buffer_received[i]) && (58>buffer_received[i]))
			address |= (buffer_received[i] - 48) << (4*(6-(i+1)));
		else
			address |= (buffer_received[i] - 55) << (4*(6-(i+1)));
	}

	UART_RTOS_Send(&handle, (uint8_t *)buffer_received, var);

	UART_RTOS_Send(&handle, (uint8_t *)secondpoint_writeMem, strlen(secondpoint_writeMem));

	UART_RTOS_Receive(&handle,buffer_received_bytes,sizeof(buffer_received_bytes),&var);
	UART_RTOS_Send(&handle, (uint8_t *)buffer_received_bytes, var);
	while(ENTER != buffer_received_bytes[0])
	{
		EEPROM_writeData(address, buffer_received_bytes[0]);
		address += 0x08;
		UART_RTOS_Receive(&handle,buffer_received_bytes,sizeof(buffer_received_bytes),&var);
		UART_RTOS_Send(&handle, (uint8_t *)buffer_received_bytes, var);
	}

	UART_RTOS_Send(&handle, (uint8_t *)completed_readMem, strlen(completed_readMem));

	while(0x1B != buffer_received_bytes[0])
	{
		UART_RTOS_Receive(&handle,buffer_received_bytes,sizeof(buffer_received_bytes),&var);
	}

}

void setUpHour()
{
	uint8_t buffer_received[8];
	uint8_t buffer_received_bytes[1];
	size_t var;
	uint8_t time = 0;

	UART_RTOS_Send(&handle, (uint8_t *)menu_setHour, strlen(menu_setHour));

	UART_RTOS_Send(&handle, (uint8_t *)firstpoint_setHour, strlen(firstpoint_setHour));
	UART_RTOS_Receive(&handle,buffer_received,sizeof(buffer_received),&var);

	RTC_setHours(((buffer_received[0]-48)<<4)|(buffer_received[1]-48));
	RTC_setMinutes(((buffer_received[3]-48)<<4)|(buffer_received[4]-48));
	RTC_setSeconds(((buffer_received[6]-48)<<4)|(buffer_received[7]-48));

	UART_RTOS_Send(&handle, (uint8_t *)buffer_received, var);

	UART_RTOS_Send(&handle, (uint8_t *)completed_setHour, strlen(completed_setHour));

	while(0x1B != buffer_received_bytes[0])
	{
		UART_RTOS_Receive(&handle,buffer_received_bytes,sizeof(buffer_received_bytes),&var);
	}

}

void setUpDate()
{
	uint8_t buffer_received[8];
	uint8_t buffer_received_bytes[1];
	size_t var;
	uint8_t time = 0;

	UART_RTOS_Send(&handle, (uint8_t *)menu_setDate, strlen(menu_setDate));

	UART_RTOS_Send(&handle, (uint8_t *)firstpoint_setDate, strlen(firstpoint_setDate));
	UART_RTOS_Receive(&handle,buffer_received,sizeof(buffer_received),&var);

	RTC_setDays(((buffer_received[0]-48)<<4)|(buffer_received[1]-48));
	RTC_setMonths(((buffer_received[3]-48)<<4)|(buffer_received[4]-48));
	RTC_setYears(((buffer_received[6]-48)<<4)|(buffer_received[7]-48));

	UART_RTOS_Send(&handle, (uint8_t *)buffer_received, var);

	UART_RTOS_Send(&handle, (uint8_t *)completed_setDate, strlen(completed_setDate));

	while(0x1B != buffer_received_bytes[0])
	{
		UART_RTOS_Receive(&handle,buffer_received_bytes,sizeof(buffer_received_bytes),&var);
	}
}

void readHour()
{
	uint8_t hora_actual [8];
	uint8_t buffer_received_bytes[1];
	size_t var;

	uint8_t seconds = RTC_getSeconds();
	uint8_t minutes = RTC_getMinutes();
	uint8_t hours = RTC_getHours();

	UART_RTOS_Send(&handle, (uint8_t *)menu_getHour, strlen(menu_getHour));
	UART_RTOS_Send(&handle, (uint8_t *)firstpoint_getHour, strlen(firstpoint_getHour));

	hora_actual[0] = ((hours & 0xF0)>>4)+48;
	hora_actual[1] = (hours & 0x0F)+48;
	hora_actual[2] = ':';
	hora_actual[3] = ((minutes & 0xF0)>>4)+48;
	hora_actual[4] = (minutes & 0x0F)+48;
	hora_actual[5] = ':';
	hora_actual[6] = ((seconds & 0x70)>>4)+48;
	hora_actual[7] = (seconds & 0x0F)+48;

	uint32_t array_length = sizeof(hora_actual)/sizeof(hora_actual[0]);

	UART_RTOS_Send(&handle, (uint8_t *)hora_actual, array_length);



	UART_RTOS_Receive(&handle,buffer_received_bytes,sizeof(buffer_received_bytes),&var);
	readHour();
}

void readDate()
{
	uint8_t fecha_actual [8];
	uint8_t buffer_received_bytes[1];
	size_t var;

	uint8_t day = RTC_getDays();
	uint8_t month = RTC_getMonths();
	uint8_t year = RTC_getYears();

	UART_RTOS_Send(&handle, (uint8_t *)menu_getDate, strlen(menu_getDate));
	UART_RTOS_Send(&handle, (uint8_t *)firstpoint_getDate, strlen(firstpoint_getDate));

	fecha_actual[0] = ((day & 0xF0)>>4)+48;
	fecha_actual[1] = (day & 0x0F)+48;
	fecha_actual[2] = '/';
	fecha_actual[3] = ((month & 0x10)>>4)+48;
	fecha_actual[4] = (month & 0x0F)+48;
	fecha_actual[5] = '/';
	fecha_actual[6] = ((year & 0xF0)>>4)+48;
	fecha_actual[7] = (year & 0x0F)+48;

	uint32_t array_length = sizeof(fecha_actual)/sizeof(fecha_actual[0]);

	UART_RTOS_Send(&handle, (uint8_t *)fecha_actual, array_length);

	while(0x1B != buffer_received_bytes[0])
	{
		UART_RTOS_Receive(&handle,buffer_received_bytes,sizeof(buffer_received_bytes),&var);
	}

}


void echoLCD()
{
	uint8_t buffer_received_bytes[1];
	size_t var;

	while(0x1B != buffer_received_bytes[0])
	{
		UART_RTOS_Receive(&handle,buffer_received_bytes,sizeof(buffer_received_bytes),&var);
		LCDNokia_sendChar(buffer_received_bytes[0]);
		UART_RTOS_Send(&handle, (uint8_t *)buffer_received_bytes, var);
	}
}


void formatHours()
{
	formatHour = RTC_getHours();

	RTC_setFormat(FORMATBIT1);

	if(formatHour > 12)
	{
		formatHour -= 12;
	}

	RTC_setHours(formatHour);
}


