/*
 * Copyright (c) 2013 - 2016, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * This is template for main module created by New Kinetis SDK 2.x Project Wizard. Enjoy!
 **/

#include <string.h>
#include "Task_Functions.h"
#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_gpio.h"
#include "task_Functions.h"
/*#include "fsl_debug_console.h"*/

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "terminal.h"
#include "timers.h"
#include "semphr.h"
#include "RTC_and_EEPROM_Functions.h"
#include "event_groups.h"
#include "General_functions.h"

/* Task priorities. */
#define menu_task_PRIORITY (configMAX_PRIORITIES - 2)
#define system_task_PRIORITY (configMAX_PRIORITIES - 1)
#define ECHO_BUFFER_LENGTH 8

//SemaphoreHandle_t xBinarySemaphore;

uint8_t g_txBuffer[ECHO_BUFFER_LENGTH] = {0};
uint8_t g_rxBuffer[ECHO_BUFFER_LENGTH] = {0};
uart_config_t uartConfig;
uart_transfer_t transfer;
uart_handle_t g_uartHandle;
uart_transfer_t sendXfer;
uart_transfer_t receiveXfer;
volatile bool rxBufferEmpty = true;
volatile bool txBufferFull = false;
volatile bool txOnGoing = false;
volatile bool rxOnGoing = false;
volatile uint8_t selector = 0;
volatile uint8_t cont = 0;

TaskHandle_t menuPrincipal_handle;
TaskHandle_t leerMemoria_handle;
TaskHandle_t escribirMemoria_handle;
TaskHandle_t establecerHora_handle;
TaskHandle_t establecerFecha_handle;
TaskHandle_t leerHora_handle;
TaskHandle_t leerFecha_handle;
TaskHandle_t dateTime_handle;

//NUEVO
SemaphoreHandle_t xMutex;
uint16_t bit [9] = {0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80, 0x100};
EventGroupHandle_t menu_event_group;
////////////////////////////////
uint8_t menuString[] = "\033[2J\033[1;1H1) Leer Memoria I2C \r\n2) Escribir memoria I2C \r\n3) Establecer Hora"
		"\r\n4) Establecer Fecha\r\n5) Formato de Hora\r\n6) Leer Hora\r\n7) Leer Fecha\r\n"
		"8) Comunicacion con terminal 2\r\n9) Eco en LCD  ";

/*
void PORTC_IRQHandler()
{
	switch(PORT_GetPinsInterruptFlags(PORTC))
	{
	case 0x01:
		GPIO_setFlagPortCX(BIT0, TRUE);
		PORT_ClearPinsInterruptFlags(PORTC, 0x1<<PB0);
		break;
	case 0x02:
		GPIO_setFlagPortCX(BIT1, TRUE);
		PORT_ClearPinsInterruptFlags(PORTC, 0x1<<PB1);
		break;
	case 0x04:
		GPIO_setFlagPortCX(BIT2, TRUE);
		PORT_ClearPinsInterruptFlags(PORTC, 0x1<<PB2);
		break;
	case 0x08:
		GPIO_setFlagPortCX(BIT3, TRUE);
		PORT_ClearPinsInterruptFlags(PORTC, 0x1<<PB3);
		break;
	case 0x10:
		GPIO_setFlagPortCX(BIT4, TRUE);
		PORT_ClearPinsInterruptFlags(PORTC, 0x1<<PB4);
		break;
	case 0x20:
		GPIO_setFlagPortCX(BIT5, TRUE);
		PORT_ClearPinsInterruptFlags(PORTC, 0x1<<PB5);
		break;
	}
}
 */

/*!
 * @brief Task responsible for printing of "Hello world." message.
 */
static void menuPrincipal_task(void *pvParameters)
{
	terminalInit();
	EventBits_t uxBits;
	uint16_t opcion;
	for(;;)
	{
		xSemaphoreTake(xMutex, portMAX_DELAY);
		opcion = listen();
		xEventGroupSetBits(menu_event_group, bit[opcion]);
		xSemaphoreGive(xMutex);
	}

}

static void leerMemoria_task(void *pvParameters)
{
	for(;;)
	{
		xEventGroupWaitBits(menu_event_group, bit[0], pdTRUE, pdFALSE, portMAX_DELAY);
		xSemaphoreTake(xMutex, portMAX_DELAY);
		readMemory();
		xSemaphoreGive(xMutex);
	}
}

static void escribirMemoria_task(void *pvParameters)
{
	for(;;)
	{
		xEventGroupWaitBits(menu_event_group, bit[1], pdTRUE, pdFALSE, portMAX_DELAY);
		xSemaphoreTake(xMutex, portMAX_DELAY);
		writeMemory();
		xSemaphoreGive(xMutex);
	}
}

static void establecerHora_task(void *pvParameters)
{
	for(;;)
	{
		xEventGroupWaitBits(menu_event_group, bit[2], pdTRUE, pdFALSE, portMAX_DELAY);
		xSemaphoreTake(xMutex, portMAX_DELAY);
		setUpHour();
		xSemaphoreGive(xMutex);
	}
}

static void establecerFecha_task(void *pvParameters)
{
	for(;;)
	{
		xEventGroupWaitBits(menu_event_group, bit[3], pdTRUE, pdFALSE, portMAX_DELAY);
		xSemaphoreTake(xMutex, portMAX_DELAY);
		setUpDate();
		xSemaphoreGive(xMutex);
	}
}

static void formatoHora_task(void *pvParameters)
{
	for(;;)
	{
		xEventGroupWaitBits(menu_event_group, bit[4], pdTRUE, pdFALSE, portMAX_DELAY);
		xSemaphoreTake(xMutex, portMAX_DELAY);
		formatHours();
		xSemaphoreGive(xMutex);

	}
}

static void leerHora_task(void *pvParameters)
{
	for(;;)
	{
		xEventGroupWaitBits(menu_event_group, bit[5], pdTRUE, pdFALSE, portMAX_DELAY);
		xSemaphoreTake(xMutex, portMAX_DELAY);
		readHour();
		xSemaphoreGive(xMutex);
	}
}

static void leerFecha_task(void *pvParameters)
{
	for(;;)
	{
		xEventGroupWaitBits(menu_event_group, bit[6], pdTRUE, pdFALSE, portMAX_DELAY);
		xSemaphoreTake(xMutex, portMAX_DELAY);
		readDate();
		xSemaphoreGive(xMutex);

	}
}

static void chat_task(void *pvParameters)
{
	for(;;)
	{

	}
}

static void dateTime_task(void *pvParameters)
{
	for(;;)
	{

	}
}

int main(void) {
	/* Init board hardware. */
	BOARD_InitPins();
	BOARD_BootClockRUN();
	NVIC_SetPriority(UART0_RX_TX_IRQn, 5);
	BOARD_InitDebugConsole();

	//NUEVO
	xMutex = xSemaphoreCreateMutex();
	menu_event_group = xEventGroupCreate();

	LCDNokia_init();
	I2C_init();

	/////////////////////////////

	/* Add your code here */
	/*
	CLOCK_EnableClock(kCLOCK_PortA);
	CLOCK_EnableClock(kCLOCK_PortC);
	CLOCK_EnableClock(kCLOCK_PortD);
	CLOCK_EnableClock(kCLOCK_PortE);

	//xBinarySemaphore = xSemaphoreCreateBinary();
	gpio_pin_config_t config_gpio =
	{ kGPIO_DigitalInput };

	port_pin_config_t config_port =
	{ kPORT_PullUp, kPORT_FastSlewRate, kPORT_PassiveFilterDisable,
			kPORT_OpenDrainDisable, kPORT_LowDriveStrength, kPORT_MuxAsGpio,
			kPORT_UnlockRegister, };

	//xQueue = xQueueCreate(2, sizeof(uint8_t[6]));

	PORT_SetPinConfig(PORTC, PUSHBUTTON5, &config_port);
	GPIO_PinInit(GPIOC, PUSHBUTTON5, &config_gpio);
	PORT_SetPinInterruptConfig(PORTC, PUSHBUTTON5, kPORT_InterruptFallingEdge);

	PORT_SetPinConfig(PORTC, PUSHBUTTON4, &config_port);
	GPIO_PinInit(GPIOC, PUSHBUTTON4, &config_gpio);
	PORT_SetPinInterruptConfig(PORTC, PUSHBUTTON4, kPORT_InterruptFallingEdge);

	PORT_SetPinConfig(PORTC, PUSHBUTTON3, &config_port);
	GPIO_PinInit(GPIOC, PUSHBUTTON3, &config_gpio);
	PORT_SetPinInterruptConfig(PORTC, PUSHBUTTON3, kPORT_InterruptFallingEdge);

	PORT_SetPinConfig(PORTC, PUSHBUTTON2, &config_port);
	GPIO_PinInit(GPIOC, PUSHBUTTON2, &config_gpio);
	PORT_SetPinInterruptConfig(PORTC, PUSHBUTTON2, kPORT_InterruptFallingEdge);

	PORT_SetPinConfig(PORTC, PUSHBUTTON1, &config_port);
	GPIO_PinInit(GPIOC, PUSHBUTTON1, &config_gpio);
	PORT_SetPinInterruptConfig(PORTC, PUSHBUTTON1, kPORT_InterruptFallingEdge);

	PORT_SetPinConfig(PORTC, PUSHBUTTON0, &config_port);
	GPIO_PinInit(GPIOC, PUSHBUTTON0, &config_gpio);
	PORT_SetPinInterruptConfig(PORTC, PUSHBUTTON0, kPORT_InterruptFallingEdge);

	NVIC_SetPriority(PORTC_IRQn, 7);
	NVIC_EnableIRQ(PORTC_IRQn);

	terminalInit();
	terminalMenu();
	 */

	/* Create RTOS task */

	xTaskCreate(leerMemoria_task, "LeerMemoria_task", 3*configMINIMAL_STACK_SIZE, NULL, system_task_PRIORITY, NULL);
	xTaskCreate(escribirMemoria_task, "EscribirMemoria_task", 3*configMINIMAL_STACK_SIZE, NULL, system_task_PRIORITY, NULL);
	xTaskCreate(establecerHora_task, "EstablecerHora_task", 3*configMINIMAL_STACK_SIZE, NULL, system_task_PRIORITY, NULL);
	xTaskCreate(establecerFecha_task, "EstablecerFecha_task", 3*configMINIMAL_STACK_SIZE, NULL, system_task_PRIORITY, NULL);
	xTaskCreate(formatoHora_task, "FormatoHora_task", configMINIMAL_STACK_SIZE, NULL, system_task_PRIORITY, NULL);
	xTaskCreate(leerHora_task, "LeerHora_task", 3*configMINIMAL_STACK_SIZE, NULL, system_task_PRIORITY, NULL);
	xTaskCreate(leerFecha_task, "LeerFecha_task", 3*configMINIMAL_STACK_SIZE, NULL, system_task_PRIORITY, NULL);
	//xTaskCreate(dateTime_task, "dateTime_task", 3*configMINIMAL_STACK_SIZE, NULL, system_task_PRIORITY, NULL);
	xTaskCreate(chat_task, "Chat_task", 3*configMINIMAL_STACK_SIZE, NULL, system_task_PRIORITY, NULL);
	xTaskCreate(menuPrincipal_task, "MenuPrincipal_task", 3*configMINIMAL_STACK_SIZE, NULL, menu_task_PRIORITY, NULL);
	/*	vTaskSuspend(leerMemoria_handle);
	vTaskSuspend(escribirMemoria_handle);
	vTaskSuspend(establecerHora_handle);
	vTaskSuspend(establecerFecha_handle);
	vTaskSuspend(leerHora_handle);
	vTaskSuspend(leerFecha_handle);*/
	//vTaskSuspend(dateTime_handle);
	vTaskStartScheduler();


	for(;;) { /* Infinite loop to avoid leaving the main function */
		__asm("NOP"); /* something to use as a breakpoint stop while looping */
	}
}



