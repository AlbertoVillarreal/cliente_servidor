/*
 * terminal.c
 *
 *  Created on: 17/03/2017
 *      Author: Alberto
 */

#include "terminal.h"

#define ECHO_BUFFER_LENGTH 8

//uart_config_t uartConfig;
uart_transfer_t transfer;
uart_handle_t g_uartHandle;

#define DEMO_UART UART0
#define DEMO_UART_CLKSRC UART0_CLK_SRC
#define DEMO_UART_RX_TX_IRQn UART0_RX_TX_IRQn
uint8_t background_buffer[32];
uint8_t recv_buffer[4];
uart_rtos_handle_t handle;
struct _uart_handle t_handle;

uart_rtos_config_t uart_config = {
		.baudrate = 115200,
		.parity = kUART_ParityDisabled,
		.stopbits = kUART_OneStopBit,
		.buffer = background_buffer,
		.buffer_size = sizeof(background_buffer),
};

uart_transfer_t sendXfer;
uart_transfer_t receiveXfer;

//NUEVO
uint8_t numMenu[1];
typedef enum{
MENU1 = 0X31,
MENU2 = 0X32,
MENU3 = 0X33,
MENU4 = 0X34,
MENU5 = 0X35,
MENU6 = 0X36,
MENU7 = 0X37,
MENU8 = 0X38,
MENU9 = 0X39
}menus;
typedef enum
{
	BIT0,
	BIT1,
	BIT2,
	BIT3,
	BIT4,
	BIT5,
	BIT6,
	BIT7,
	BIT8,
	BIT9
}bits;

/////////////////////////////7


const char *menuString = "\033[2J\033[1;1H1) Leer Memoria I2C \r\n2) Escribir memoria I2C \r\n3) Establecer Hora"
		"\r\n4) Establecer Fecha\r\n5) Formato de Hora\r\n6) Leer Hora\r\n7)Leer Fecha\r\n"
		"8) Comunicacion con terminal 2\r\n9) Eco en LCD";

const char *mensaje = "\033[2J\033[1;1H1) Leer Memoria I2C \r\n2) Escribir memoria I2C \r\n3) Establecer Hora"
		"\r\n4) Establecer Fecha\r\n5) Formato de Hora\r\n6) Leer Hora\r\n7)Leer Fecha\r\n"
		"8) Comunicacion con terminal 2\r\n9) Eco en LCD";
const char *leerMemoria = "\033[2J\033[1;1HDireccion de lectura";

uint8_t dir[] = "0x0000";

volatile bool rxBufferEmpty = true;
volatile bool txBufferFull = false;
volatile bool txOnGoing = false;
volatile bool rxOnGoing = false;
volatile uint8_t selector = 0;
volatile bool direccion = false;
volatile bool longitudB = false;
volatile uint8_t cont = 0;

uint8_t g_txBuffer[ECHO_BUFFER_LENGTH] = {0};
uint8_t g_rxBuffer[ECHO_BUFFER_LENGTH] = {0};
bool leerMemoriaFlag = false;
bool escribirMemoriaFlag = false;
bool establecerHoraFlag = false;
bool establecerFechaFlag = false;
bool formatoHoraFlag = false;


void UART_UserCallback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData);

void UART_UserCallback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData)
{
	userData = userData;
	int data;

	if (kStatus_UART_TxIdle == status)
	{
		txBufferFull = false;
		txOnGoing = false;
	}

	if (kStatus_UART_RxIdle == status)
	{
		rxBufferEmpty = false;
		rxOnGoing = false;
	}
}
/*
void UART0_RX_TX_IRQHandler(void)
{
	uint8_t data;

	if ((kUART_RxDataRegFullFlag | kUART_RxOverrunFlag) & UART_GetStatusFlags(UART0))
	{
		data = UART_ReadByte(UART0);


	}
}*/

//NUEVO
uint16_t listen()
{
	size_t n;
	uint16_t menu;

	UART_RTOS_Send(&handle, (uint8_t *)mensaje, strlen(mensaje));
	UART_RTOS_Receive(&handle, numMenu, sizeof(numMenu), &n);
	switch(numMenu[0])
	{
	case MENU1:
		menu = BIT0;
		break;
	case MENU2:
		menu = BIT1;
		break;
	case MENU3:
		menu = BIT2;
		break;
	case MENU4:
		menu = BIT3;
		break;
	case MENU5:
		menu = BIT4;
		break;
	case MENU6:
		menu = BIT5;
		break;
	case MENU7:
		menu = BIT6;
		break;
	case MENU8:
		menu = BIT7;
		break;
	case MENU9:
		menu = BIT8;
		break;
	default: BIT9;
	}
	return menu;
}
//////////////////////////////

void terminalInit()
{
	/*
	UART_GetDefaultConfig(&uartConfig);
	uartConfig.baudRate_Bps = BOARD_DEBUG_UART_BAUDRATE;
	uartConfig.enableTx = true;
	uartConfig.enableRx = true;
	UART_Init(UART0, &uartConfig, CLOCK_GetFreq(UART0_CLK_SRC));
	UART_TransferCreateHandle(UART0, &g_uartHandle, UART_UserCallback, NULL);
	UART_EnableInterrupts(UART0, kUART_RxDataRegFullInterruptEnable | kUART_RxOverrunInterruptEnable);
	EnableIRQ(UART0_RX_TX_IRQn);*/


	uart_config.srcclk = CLOCK_GetFreq(DEMO_UART_CLKSRC);
	uart_config.base = DEMO_UART;

	UART_RTOS_Init(&handle, &t_handle, &uart_config);

}

void terminalMenu()
{
	/* If RX is idle and g_rxBuffer is empty, start to read data to g_rxBuffer. */
	if ((!rxOnGoing) && rxBufferEmpty)
	{
		rxOnGoing = true;
		UART_TransferReceiveNonBlocking(DEMO_UART, &g_uartHandle, &receiveXfer, NULL);
	}

	/* If TX is idle and g_txBuffer is full, start to send data. */
	if ((!txOnGoing) && txBufferFull)
	{
		txOnGoing = true;
		UART_TransferSendNonBlocking(DEMO_UART, &g_uartHandle, &sendXfer);
	}

	/* If g_txBuffer is empty and g_rxBuffer is full, copy g_rxBuffer to g_txBuffer. */
	if ((!rxBufferEmpty) && (!txBufferFull))
	{
		memcpy(g_txBuffer, g_rxBuffer, ECHO_BUFFER_LENGTH);
		rxBufferEmpty = true;
		txBufferFull = true;
	}

}


void escribirMenu(uint8_t opcion[])
{
	uint8_t i = 0;
	while(opcion[i]!=0)
	{
		i++;
	}
	transfer.data = opcion;
	transfer.dataSize = i - 1;
	txOnGoing = true;
	UART_TransferSendNonBlocking(UART0, &g_uartHandle, &transfer);
	while (txOnGoing)
	{
	}

	g_rxBuffer[0] = 0;
}


void formatoHora(uint8_t hora[])
{
	escribirMenu("\033[3;18HEl formato ha sido cambiado...");
}

void leerHora()
{
	//Inserrtar la hora.
	escribirMenu("\033[2J\033[1;1HLa hora actual es:");
}

void leerFecha()
{
	//Inserrtar la fecha.
	escribirMenu("\033[2J\033[1;1HLa fecha actual es:");
}


