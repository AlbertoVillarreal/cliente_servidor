/*
 * terminal.h
 *
 *  Created on: 17/03/2017
 *      Author: Alberto
 */

#ifndef SOURCE_TERMINAL_H_
#define SOURCE_TERMINAL_H_

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "fsl_gpio.h"
#include "board.h"

#include "fsl_uart.h"
#include "fsl_uart_freertos.h"

#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_debug_console.h"
#include "fsl_device_registers.h"


void terminalInit();
void terminalMenu();
uint16_t listen();
void terminalCambioMenu();
void terminalOpciones();
void escribirMenu(uint8_t opcion[]);
void UART_UserCallback(UART_Type *base, uart_handle_t *handle, status_t status, void *userData);

void formatoHora(uint8_t hora[]);
void ecoLCD(uint8_t LCD[]);


#endif /* SOURCE_TERMINAL_H_ */
